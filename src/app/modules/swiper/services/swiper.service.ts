import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {User} from '../../profile/models/User';
import {UserService} from '../../profile/services/user.service';

@Injectable({
  providedIn: 'root'
})
export class SwiperService {

  constructor(private http: HttpClient, private userService: UserService) {}

  public getNextUser(): Promise<User> {
    return new Promise((resolve, reject) => {
      this.http.get<User>(`${environment.apiUrl}users/next?currentUserId=${this.userService.user.id}`)
        .subscribe(user => {
          if (user.images.length === 0) {
            user.images.push({
              userId: user.id,
              id: 0,
              url: 'https://media.istockphoto.com/vectors/default-profile-picture-avatar-photo-placeholder-vector-illustration-vector-id1223671392?b=1&k=6&m=1223671392&s=612x612&w=0&h=5VMcL3a_1Ni5rRHX0LkaA25lD_0vkhFsb1iVm1HKVSQ='
            });
          }
          resolve(user);
        }, error => {
          reject(error.error);
        });
    });
  }

  public Like(affectedUser: number): void {
    const body: object = {
      currentUser: this.userService.user.id,
      affectedUser,
    };
    // @ts-ignore
    this.http.post<void>(`${environment.apiUrl}users/like`, body, {responseType: 'text'})
      .subscribe(() => {});
  }

  public Dislike(affectedUser: number): void {
    const body: object = {
      currentUser: this.userService.user.id,
      affectedUser,
    };
    // @ts-ignore
    this.http.post<void>(`${environment.apiUrl}users/dislike`, body, {responseType: 'text'})
      .subscribe(() => {});
  }
}
