import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SwiperRoutingModule} from './swiper-routing.module';
import {SwiperComponent} from './components/swiper/swiper.component';
import {ImageStepBtnComponent} from './components/image-step-btn/image-step-btn.component';
import {ImageChangeBtnComponent} from './components/image-change-btn/image-change-btn.component';
import {SwipeBtnComponent} from './components/swipe-btn/swipe-btn.component';
import {SwiperContentComponent} from './components/swiper-content/swiper-content.component';
import {SwiperImagesComponent} from './components/swiper-images/swiper-images.component';
import {HttpClientModule} from '@angular/common/http';


@NgModule({
  declarations: [
    SwiperComponent,
    ImageStepBtnComponent,
    ImageChangeBtnComponent,
    SwipeBtnComponent,
    SwiperContentComponent,
    SwiperImagesComponent
  ],
  imports: [
    CommonModule,
    SwiperRoutingModule,
    HttpClientModule,
  ]
})
export class SwiperModule {
}
