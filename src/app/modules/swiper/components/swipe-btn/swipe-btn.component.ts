import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-swipe-btn',
  templateUrl: './swipe-btn.component.html',
})
export class SwipeBtnComponent implements OnInit {
  @Input() action: string;

  @Output() clickAction = new EventEmitter<any>();

  constructor() { }

  ngOnInit(): void {
  }

  getClassesByAction(): string {
    if (this.action === 'dislike') {
      return 'bg-red-600 hover:bg-red-700';
    }

    if (this.action === 'like') {
      return 'bg-green-600 hover:bg-green-700';
    }

  }

}
