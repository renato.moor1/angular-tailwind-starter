import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Image} from '../../../profile/models/Image';

@Component({
  selector: 'app-image-step-btn',
  templateUrl: './image-step-btn.component.html',
})
export class ImageStepBtnComponent implements OnInit {
  @Input() images: Array<Image>;
  @Input() image: Image;

  @Output() change = new EventEmitter<number>();

  constructor() { }

  ngOnInit(): void {
  }

}
