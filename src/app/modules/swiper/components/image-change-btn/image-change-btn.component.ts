import {Component, HostBinding, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {Image} from '../../../profile/models/Image';

@Component({
  selector: 'app-image-change-btn',
  templateUrl: './image-change-btn.component.html',
})
export class ImageChangeBtnComponent implements OnInit {
  @Input() direction: string;
  @Input() images: Array<Image>;
  @Input() image: Image;

  @Output() change = new EventEmitter<number>();

  constructor() { }
  @HostBinding('class')
  // tslint:disable-next-line:typedef
  get themeClass() {
    return `my-auto`;
  }
  ngOnInit(): void {
  }

  show(): boolean {
    if (this.images.indexOf(this.image) === 0 && this.direction === 'left') {
      return false;
    }
    if (this.images.indexOf(this.image) === this.images.length - 1 && this.direction === 'right') {
      return false;
    }
    return true;
  }
}
