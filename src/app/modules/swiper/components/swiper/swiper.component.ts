import {Component, HostBinding, OnInit} from '@angular/core';
import {SwiperService} from '../../services/swiper.service';
import {Image} from '../../../profile/models/Image';

@Component({
  selector: 'app-swiper',
  templateUrl: './swiper.component.html',
})
export class SwiperComponent implements OnInit {
  images: Array<Image>;
  image: Image;
  id: number;
  like: boolean;
  dislike: boolean;
  disappear: boolean;
  preAppear: boolean;
  appear: boolean;
  name: string;
  bio: string;
  noUsers: boolean;

  constructor(private swiperService: SwiperService) {
    this.noUsers = false;
    this.images = [];
    this.image = {
      userId: 0,
      id: 0,
      url: 'https://media.istockphoto.com/vectors/default-profile-picture-avatar-photo-placeholder-vector-illustration-vector-id1223671392?b=1&k=6&m=1223671392&s=612x612&w=0&h=5VMcL3a_1Ni5rRHX0LkaA25lD_0vkhFsb1iVm1HKVSQ='
    };
    this.images.push(this.image);
    this.nextFisher();
  }

  ngOnInit(): void {
    this.like = false;
    this.dislike = false;
    this.disappear = false;
    this.preAppear = false;
    this.appear = false;
  }

  @HostBinding('class')
  // tslint:disable-next-line:typedef
  get themeClass() {
    return `flex-grow content-center flex flex-wrap h-full justify-center`;
  }

  nextImage(): void {
    this.image = this.images[this.images.indexOf(this.image) + 1];
  }

  previousImage(): void {
    this.image = this.images[this.images.indexOf(this.image) - 1];
  }

  selectImage(index: number): void {
    this.image = this.images[index];
  }

  likeFisher(): void {
    this.swiperService.Like(this.id);
    this.animation('like');
  }

  dislikeFisher(): void {
    this.swiperService.Dislike(this.id);
    this.animation('dislike');
  }

  animation(type: any): void {
    this.appear = false;
    this[type] = true;
    setTimeout(() => {
      this.disappear = true;
      this[type] = false;
      this.nextFisher();
      setTimeout(() => {
        this.preAppear = true;
        this.disappear = false;
        this.appear = true;
      }, 150);
    }, 200);
  }

  nextFisher(): void {
    this.swiperService.getNextUser().then(user => {
        this.id = user.id;
        this.images = user.images;
        this.image = user.images[0];
        this.bio = user.bio;
        this.name = user.name;
      }
    ).catch(err => {
      this.noUsers = true;
    });
  }
}
