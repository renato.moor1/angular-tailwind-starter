import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-swiper-images',
  templateUrl: './swiper-images.component.html',
})
export class SwiperImagesComponent implements OnInit {
  @Input() image: string;
  constructor() { }

  ngOnInit(): void {}
}
