import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-swiper-content',
  templateUrl: './swiper-content.component.html',
})
export class SwiperContentComponent implements OnInit {
  @Input() name: string;
  @Input() bio: string;

  constructor() { }

  ngOnInit(): void {}
}
