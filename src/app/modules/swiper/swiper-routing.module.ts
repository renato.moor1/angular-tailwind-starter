import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {SwiperComponent} from './components/swiper/swiper.component';
import {AuthGuard} from '../authentication/guards/auth.guard';

const routes: Routes = [
  { path: '', component: SwiperComponent, canActivate: [AuthGuard] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class SwiperRoutingModule { }
