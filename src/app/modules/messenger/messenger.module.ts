import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MessengerRoutingModule } from './messenger-routing.module';
import { MatchesComponent } from './components/matches/matches.component';
import { MatchCardComponent } from './components/match-card/match-card.component';



@NgModule({
  declarations: [MatchesComponent, MatchCardComponent],
  imports: [
    CommonModule,
    MessengerRoutingModule
  ]
})
export class MessengerModule { }
