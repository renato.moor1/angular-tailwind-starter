import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MatchesComponent} from './components/matches/matches.component';
import {AuthGuard} from '../authentication/guards/auth.guard';

const routes: Routes = [
  { path: 'matches', component: MatchesComponent, canActivate: [AuthGuard] }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class MessengerRoutingModule { }
