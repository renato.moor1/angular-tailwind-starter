import {Component, Input, OnInit} from '@angular/core';
import {ImageService} from '../../../profile/services/image.service';

@Component({
  selector: 'app-match-card',
  templateUrl: './match-card.component.html',
})
export class MatchCardComponent implements OnInit {
  @Input() name: string;
  @Input() img: string;

  constructor(private imageService: ImageService) { }

  ngOnInit(): void {}

}
