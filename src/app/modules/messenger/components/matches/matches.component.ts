import {Component, OnInit} from '@angular/core';
import {UserService} from '../../../profile/services/user.service';

@Component({
  selector: 'app-matches',
  templateUrl: './matches.component.html',
})
export class MatchesComponent implements OnInit {

  matches: Array<object>;

  constructor(private userService: UserService) {}

  ngOnInit(): void {
    this.getMatches();
  }

  getMatches(): void {
    this.userService.refreshUserData().then(
      user => this.matches = user.matches
    );
  }

}
