import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileRoutingModule } from './profile-routing.module';
import { ProfileComponent } from './components/profile/profile.component';
import { FormSectionComponent } from './components/form-section/form-section.component';
import { FormImageComponent } from './components/form-image/form-image.component';
import {FormsModule} from '@angular/forms';



@NgModule({
  declarations: [ProfileComponent, FormSectionComponent, FormImageComponent],
  imports: [
    CommonModule,
    ProfileRoutingModule,
    FormsModule
  ]
})
export class ProfileModule { }
