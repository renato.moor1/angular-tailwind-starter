import { Component, OnInit } from '@angular/core';
import {ImageService} from '../../services/image.service';
import {UserService} from '../../services/user.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html'
})
export class ProfileComponent implements OnInit {
  constructor(private imageService: ImageService, public userService: UserService) { }
  name: string;
  bio: string;
  ngOnInit(): void {
    this.name = this.userService.user.name;
    this.bio = this.userService.user.bio;
  }

  uploadImage = (file: any)  => {
    this.imageService
      .AddImage(file.target.files[0])
      .catch(err => console.log('ERROR', err));
  }

  changeName(): void {
    this.userService.changeUserName(this.name);
  }

  changeBio(): void {
    this.userService.changeBio(this.bio);
  }

}
