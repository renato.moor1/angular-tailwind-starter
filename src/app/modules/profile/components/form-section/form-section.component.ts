import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-form-section',
  templateUrl: './form-section.component.html'
})
export class FormSectionComponent implements OnInit {
  @Input() title: string;

  constructor() { }

  ngOnInit(): void {
  }

}
