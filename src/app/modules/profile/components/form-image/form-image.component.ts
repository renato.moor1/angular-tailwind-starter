import {Component, Input, OnInit} from '@angular/core';
import {ImageService} from '../../services/image.service';

@Component({
  selector: 'app-form-image',
  templateUrl: './form-image.component.html'
})
export class FormImageComponent implements OnInit {
  @Input() url: string;
  @Input() id: number;
  loading: boolean;
  constructor(public imageService: ImageService) {
  }

  ngOnInit(): void {}

  delete(): void {
    this.imageService.delete(this.id);
  }
}
