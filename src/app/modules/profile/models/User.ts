import {Image} from './Image';

export interface User {
  id: number;
  images: Array<Image>;
  matches: Array<any>;
  name: string;
  bio: string;
}
