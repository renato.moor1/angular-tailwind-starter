export interface Image {
  id: number;
  url: string;
  userId: number;
}
