import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {User} from '../models/User';
import {environment} from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  user: User;

  constructor(private http: HttpClient) {}

  refreshUserData(): Promise<User> {
    return new Promise((resolve, reject) => {
      // @ts-ignore
      this.http.get<User>(`${environment.apiUrl}users/${this.user.id}`).subscribe(user => {
        this.user = user;
        resolve(user);
      });
    });

  }

  changeUserName(name: string): Promise<any> {
    const body = {
      currentUser: this.user.id,
      name,
    };

    return new Promise((resolve, reject) => {
      // @ts-ignore
      this.http.post<string>(`${environment.apiUrl}users/update/name`, body, {responseType: 'text'})
        .subscribe((res) => {
          this.refreshUserData();
        });
    });
  }

  changeBio(bio: string): Promise<any> {
    const body = {
      currentUser: this.user.id,
      bio,
    };

    return new Promise((resolve, reject) => {
      // @ts-ignore
      this.http.post<string>(`${environment.apiUrl}users/update/bio`, body, {responseType: 'text'})
        .subscribe((res) => {
          this.refreshUserData();
        });
    });
  }
}
