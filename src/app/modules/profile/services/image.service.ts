import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {UserService} from './user.service';
import rokka0 from 'rokka';

const rokka = rokka0({
  apiKey: 'wtnMb7m4eoUmHshDCGCPNQ5bJZuT39mj'
});

@Injectable({
  providedIn: 'root'
})

export class ImageService {
  constructor(private http: HttpClient, private userService: UserService) {
  }

  async delete(id: number): Promise<void> {
    return new Promise((resolve, reject) => {
      // @ts-ignore
      this.http.delete<void>(`${environment.apiUrl}images/${id}`, {responseType: 'text'})
        .subscribe((response) => {
          this.userService.refreshUserData();
          resolve();
        });
    });
  }

  async AddImage(file: File): Promise<any> {
    return new Promise((resolve, reject) => {
      rokka.sourceimages.create('fishingtest', file.name, file)
        .then((result) => {
          if (result.body.items.length > 0) {
            const addedFile = result.body.items[0];
            const fileUrl = `https://fishingtest.rokka.io/dynamic/noop/${addedFile.hash}.${addedFile.format}`;
            this.UploadImage(fileUrl);
            resolve(fileUrl);
          }
        })
        .catch((err) => reject(err));
    });
  }

  async UploadImage(url: string): Promise<any> {
    const body: object = {
      userId: this.userService.user.id,
      url,
    };
    return new Promise((resolve, reject) => {
      // @ts-ignore
      this.http.post<void>(`${environment.apiUrl}images/create`, body, {responseType: 'text'})
        .subscribe(() => {
          this.userService.refreshUserData();
          resolve();
        });
    });
  }

  async GetUserImages(userId: number): Promise<any> {
    return new Promise((resolve) => {
      // @ts-ignore
      this.http.get<any>(`${environment.apiUrl}images?userId=${userId}`)
        .subscribe((res) => {
          resolve(res);
        });
    });
  }
}
