import {Component, HostBinding, OnInit} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {
  errors: Array<string>;
  userName: string;
  password: string;

  constructor(private authService: AuthService, private router: Router) {
  }

  @HostBinding('class')
  // tslint:disable-next-line:typedef
  get themeClass() {
    return `flex-grow content-center flex flex-wrap h-full justify-center`;
  }

  ngOnInit(): void {
    this.errors = [];
  }

  login(): void {
    this.errors = [];
    this.authService.login(this.userName, this.password).then(() => {
      if (this.authService.isAuthenticated === true) {
        this.router.navigate(['/']);
      }
    }).catch(() => {
      this.errors.push(`Credentials not valid`);
    });

  }
}
