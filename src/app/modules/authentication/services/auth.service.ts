import {Injectable} from '@angular/core';
import {UserLogin} from '../models/UserLogin';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {User} from '../../profile/models/User';
import {UserService} from '../../profile/services/user.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  userLogin: UserLogin;
  isAuthenticated: boolean;

  constructor(private http: HttpClient, private userService: UserService) {
    this.userLogin = {
      username: '',
      password: ''
    };
    this.isAuthenticated = false;
  }

  async login(username: string, password: string): Promise<void> {
    this.userLogin = {
      username,
      password,
    };
    return new Promise((resolve, reject) => {
      this.http.post<User>(`${environment.apiUrl}users/login`, this.userLogin)
        .subscribe((user) => {
          if (user) {
            this.userService.user = user;
            this.isAuthenticated = true;
            resolve();
          } else {
            reject();
          }
        });
    });
  }

  logout(): void {
    this.isAuthenticated = false;
  }
}
