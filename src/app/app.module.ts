import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {NavbarComponent} from './components/navbar/navbar.component';
import {NavButtonComponent} from './components/navbar/components/button/nav-button.component';
import {MessengerModule} from './modules/messenger/messenger.module';
import {ProfileModule} from './modules/profile/profile.module';
import {SwiperModule} from './modules/swiper/swiper.module';
import {AuthenticationModule} from './modules/authentication/authentication.module';

@NgModule({
  imports: [
    BrowserModule,
    AppRoutingModule,
    MessengerModule,
    ProfileModule,
    SwiperModule,
    AuthenticationModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
  declarations: [AppComponent, NavbarComponent, NavButtonComponent],
})
export class AppModule {
}
