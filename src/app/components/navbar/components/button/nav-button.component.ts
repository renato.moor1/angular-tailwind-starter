import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-nav-button',
  templateUrl: './nav-button.component.html',
})
export class NavButtonComponent implements OnInit {
  @Input() text: string;
  @Input() href: string;
  @Input() position: string;

  constructor() {
  }

  ngOnInit(): void {}

  getDirectionClasses(): string {
    if (this.position === 'left') {
      return 'rounded-b rounded-r';
    }
    if (this.position === 'right') {
      return 'rounded-b rounded-l';
    }
    if (this.position === 'center') {
      return 'rounded-md';
    }
  }

}
